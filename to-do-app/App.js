import React from 'react';
import { StyleSheet, Text, View, AsyncStorage, Button } from 'react-native';
import  Header  from './Header';
import  Body  from './Body';

export default class App extends React.Component {
  constructor(){
    super();
    this.state = {tareas: [],
    texto: "",
    cargando: true,
    };
  }

/*   Metodo que se ejecuta cuando al menos 
  una vez se ejecuta el metodo render */
  componentDidMount(){
    this.recuperarEnTelefono();
  }

  establecerTetxto = (value) =>{
    console.log(value);
    this.setState({ texto: value });
  }

  agregarTarea = () =>{
    const nuevasTareas = [...this.state.tareas, {texto: this.state.texto, key:  Date.now().toString() }]
    this.guardarEnTelefono(nuevasTareas);
    this.setState({
      //operador ... saca cada elemnto del array
      // tareas: [...this.state.tareas, {texto: this.state.texto, key:  Date.now().toString() }],
      tareas: nuevasTareas,
      texto: '',
    });
  }

  eliminarTarea = (id) =>{
    const nuevasTareas = this.state.tareas.filter((tarea) =>{
      return tarea.key != id;
    });
    this.guardarEnTelefono(nuevasTareas);
    this.setState({
      tareas : nuevasTareas,
    })
  }

 /*  AsyncStorage.setItem("@appCursoUdemy:tareas",JSON.stringify([{key:1,texto:'uno'}
    ,{key:2,texto:'dos'}])) */
  //Guardar en el tefono
  guardarEnTelefono = (tareas) =>{
    // Utilizamos AsyncStorage, damos una llave y el valor
    AsyncStorage.setItem("@appCursoUdemy:tareas",JSON.stringify(tareas))
      .then((valor) =>{//promesa
        console.log(valor);
      })
      .catch((error) =>{
        console.log(error);
      });
  }

  //Recuperar en el telefono
  recuperarEnTelefono = () =>{
    // recuepramos por la llave
    AsyncStorage.getItem("@appCursoUdemy:tareas")
      .then((valor) =>{//promesa
        console.log(valor);
        console.log(JSON.parse(valor));
        //Creamos un setTimeOut paracontrolar
        //el tiempo de nuestra carga
        setTimeout(()=>{
          this.setState({
            cargando : false,
          })
        }, 5000);
  
        if(valor != null){
          const nuevasTareas = JSON.parse(valor);
          this.setState({
            tareas : nuevasTareas,
          });
        }
      })
      .catch((error) =>{
        console.log(error);
        this.setState({
          cargando : false,
        })
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Header cambiarTexto={this.establecerTetxto}
          agregar={this.agregarTarea}
          texto={this.state.texto}/>
        <Body tareas={this.state.tareas}
          eliminar={this.eliminarTarea}
          cargando={this.state.cargando}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
});

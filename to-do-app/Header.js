import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

class Header extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput
        style={styles.texto}
        // onChangeText={ (value) =>{console.log(value)}}
        onChangeText={this.props.cambiarTexto}
        onSubmitEditing={this.props.agregar}
        placeholder="Aqui escribe el texto.."
        value={this.props.texto}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#1976D2',
    justifyContent: 'center',
  },
  texto:{
    paddingHorizontal: 16,
    fontSize: 24, 
  }
});

export default Header;

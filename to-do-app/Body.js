import React from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import  Tarea  from "./Tarea";

class Body extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {/* funcion para activar el loading */}
        {this.props.cargando &&
          <ActivityIndicator
          size="large"
          color="#1976D2"
          />
        }

        {/* No mostar las tareas cuando el loading este ejecutandose */}
        { !this.props.cargando &&
        
           <FlatList 
          data={this.props.tareas}
          // Descontruir un elemento {}
          //renderItem={({item}) => <Text>{item.texto}</Text>}
          renderItem={({item}) => <Tarea item={item}
          eliminar={this.props.eliminar}
          />}
        />
        
        }
        
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 9,
    backgroundColor: '#BBDEFB',
  },
});

export default Body;
